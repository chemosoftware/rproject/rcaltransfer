# Calibration transfer for chemometrics and spectral data applications

This package contains methods to perform calibration transfer based on bilinear models, mainly Partial Least Squares Regression.
Package [rchemo](https://github.com/mlesnoff/rchemo) is mandatory.

The methods included are:

(Piecewise) Direct Standardization (PDS, DS) (Wang 1991, Bouveresse1996)

Orthogonal projection (EPO transfer) (Zeaiter 2006, Roger 2003)

Domain invariant PLS (Nikzad-Langerodi 2018, 2020)

Joint Y PLS (Folch-Fortuny 2017, Garcia Munoz 2005)

1. Install main dependency.
Install first the 'rchemo' package. For this, in R (or Rstudio). You may need other dependencies for this package. Please check the [rchemo installation instructions](https://github.com/mlesnoff/rchemo). For simple install, try:
```r
install.packages("devtools")
devtools::install_github("mlesnoff/rchemo", dependencies = TRUE, build_vignettes = TRUE)
```
2. Install 'rcaltransfer'. After step 1, run:
```r
devtools::install_gitlab("chemosoftware/rproject/rcaltransfer")
```
or 
```r
devtools::install_git("https://gitlab.com/chemsoftware/rproject/rcaltransfer.git")
```


3. To access documentation of functions, run:
```r
library(rcaltransfer)
# require(rcaltransfer)
?sbc
?dipls_nipals
?ds_svd
?epo_svd
?jointy_reg  
?pds_plsr  
```
